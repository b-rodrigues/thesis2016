(TeX-add-style-hook
 "main"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("Thesis" "11pt" "a4paper" "oneside")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("xcolor" "table") ("inputenc" "utf8") ("natbib" "sectionbib" "round") ("eurosym" "gen") ("titlesec" "small" "sf" "bf")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "latex2e"
    "Chapters/Chapter1"
    "Chapters/Chapter2"
    "Chapters/Chapter3"
    "Chapters/Chapter4"
    "Chapters/Chapter5"
    "Chapters/Chapter6"
    "Thesis"
    "Thesis11"
    "xcolor"
    "inputenc"
    "natbib"
    "chapterbib"
    "todonotes"
    "dirtytalk"
    "eurosym"
    "rotating"
    "tikz"
    "float"
    "dsfont"
    "cleveref"
    "threeparttable"
    "booktabs"
    "titlesec"
    "longtable"
    "array"
    "caption"
    "graphicx"
    "siunitx"
    "multirow"
    "hhline"
    "calc"
    "tabularx")
   (TeX-add-symbols
    '("todoin" ["argument"] 1)
    "HRule"))
 :latex)

