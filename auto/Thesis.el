(TeX-add-style-hook
 "Thesis"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("caption" "centerlast" "small" "sc") ("subfig" "scriptsize") ("hyperref" "pdfpagemode={UseOutlines}" "bookmarks=true" "bookmarksopen=true" "bookmarksopenlevel=0" "bookmarksnumbered=true" "hypertexnames=false" "colorlinks" "linkcolor={blue}" "citecolor={blue}" "urlcolor={red}" "pdfstartview={FitV}" "unicode" "breaklinks=true")))
   (add-to-list 'LaTeX-verbatim-environments-local "lstlisting")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "path")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "url")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "nolinkurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperbaseurl")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperimage")
   (add-to-list 'LaTeX-verbatim-macros-with-braces-local "hyperref")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "lstinline")
   (add-to-list 'LaTeX-verbatim-macros-with-delims-local "path")
   (TeX-run-style-hooks
    "setspace"
    "vmargin"
    "inputenc"
    "fancyhdr"
    "amsmath"
    "amsfonts"
    "amssymb"
    "amscd"
    "amsthm"
    "xspace"
    "caption"
    "graphicx"
    "epstopdf"
    "subfig"
    "booktabs"
    "rotating"
    "listings"
    "lstpatch"
    "hyperref"
    "longtable")
   (TeX-add-symbols
    '("dedicatory" 1)
    '("acknowledgements" 1)
    '("listofnomenclature" 2)
    '("listofconstants" 2)
    '("listofsymbols" 2)
    '("addtotoc" 1)
    '("keywords" 1)
    '("subject" 1)
    '("FACULTY" 1)
    '("faculty" 1)
    '("GROUP" 1)
    '("group" 1)
    '("DEPARTMENT" 1)
    '("department" 1)
    '("UNIVERSITY" 1)
    '("university" 1)
    '("addresses" 1)
    '("authors" 1)
    '("degree" 1)
    '("examiner" 1)
    '("thesistitle" 1)
    '("cosupervisor" 1)
    '("supervisor" 1)
    '("aref" 1)
    '("sref" 1)
    '("cref" 1)
    '("eref" 1)
    '("tref" 1)
    '("fref" 1)
    '("btypeout" 1)
    '("Declaration" 1)
    "bhrule"
    "listsymbolname"
    "listconstants"
    "listnomenclature"
    "baseclass"
    "today"
    "cleardoublepage"
    "supname"
    "cosupname"
    "ttitle"
    "examname"
    "degreename"
    "authornames"
    "addressnames"
    "univname"
    "UNIVNAME"
    "deptname"
    "DEPTNAME"
    "groupname"
    "GROUPNAME"
    "facname"
    "FACNAME"
    "subjectname"
    "keywordnames"
    "footnotesize"
    "footnoterule"
    "thanks"
    "maketitle"
    "title"
    "author"
    "date"
    "and")
   (LaTeX-add-environments
    "abstract")
   (LaTeX-add-counters
    "dummy")
   (LaTeX-add-amsthm-newtheorems
    "example"
    "theorem"
    "corollary"
    "lemma"
    "proposition"
    "axiom"
    "definition"
    "remark")
   (LaTeX-add-listings-lstdefinestyles
    "matlab"))
 :latex)

