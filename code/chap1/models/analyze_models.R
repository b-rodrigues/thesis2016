library("tidyverse")
library("janitor")
library("sjPlot")


# To install sjPlot:
# update.packages()
# install.packages("Matrix")
# install.packages("TMB", type="source")
# install.packages("lmtest")
# restart R session
# install.packages("sjPlot")

setwd("/home/bro/Documents/thesis2016/code/chap1/models/")

mod1 = readRDS("mod_11_december.rds")

# Plot random effects
re_mod1 = plot_model(mod1, type = "re")

walk2(list("re_1.pdf", "re_2.pdf"), re_mod1, ggsave)



map2(.y = re_mod1, .x = seq(1, length(re_mod1)),
     ~ggsave(filename = paste0("fixed_effects", .x, ".pdf"), .y))

ggsave("re_mod1.pdf", re_mod1, device = "pdf")

pred_data = tibble::tribble(
                      ~`(Intercept)`, ~immigrImmigrant,
                      -2.8, 1
                    )

# Plot fixed effects
est_mod1 = plot_model(mod1, type = "est")

model_assumptions = sjp.glmer(mod1, type = "ma")
