(TeX-add-style-hook
 "Chapter1"
 (lambda ()
   (LaTeX-add-labels
    "chap1")
   (LaTeX-add-bibliographies
    "bibliography"))
 :latex)

