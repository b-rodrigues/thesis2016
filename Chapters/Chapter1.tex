% Chapter 1

\chapter{Introduction}
\label{chap1}
% Main chapter title

\lhead{Chapter 1. \emph{Introduction: Description of the data}}

\section{Short description of the different chapters.}

This thesis deals with labour supply and childbearing decisions of women in France.
Each chapter corresponds to a research paper; each of these research papers contains a detailed literature
review. This introduction will discuss the different data sources that were used in detail
to make this thesis possible. Indeed, a very important element of empirical work
is obviously data availability. The previous sentence seems self-evident, however, access to data
is still a major hurdle that empiricists have to overcome. This introductory chapter will discuss
systems that allow researchers to access data and then present the datasets used in most of
the papers in greater detail. The last paper of this thesis discusses the notion of version
control by reviewing and presenting Git. In the last part of the introduction, I will explain
what this tool called Git is and why I discuss this topic in this thesis.

\section{Access to data for empirical studies in economics}

Economists, and other social scientists, face a major hurdle not encountered in other disciplines
such as the life sciences: access to high quality data to conduct empirical studies. In life
sciences and other disciplines, such as machine learning, finding data can be a complex task, but
often the researchers are not confronted to administrative hurdles. For example, a data scientist interested
in sentiment analysis after a political debate, can simply scrape data from Twitter, or from any
other social network. A physicist interested in the behaviour of a certain particle would \emph{only} need
to observe and use tools to measure that particle. The precendent sentence is an
oversimplification, of course, as there certainly are also legal and administrative hurdles.
Economists however, and mostly micro-economists, face a very practical problem concerning data:
privacy issues. A labour economist interested in female labour supply for instance, would have to
have data on thousands of women's wages, education levels, fertility, and other such variables,
most of which might be sensitive.
An economist interested in industrial organization would need to have access to accounting data on
thousands (or much more) of firms. This is usually very sensitive data that people and
organizations are very reluctant to hand over. This means that access to this type of data is in
general regulated and only possible within a very specific research project. To further protect
individuals, a new European Regulation, the \emph{General Data Protection Regulation} which came
into force in May 2018 gives much more control to individuals over their personal data. Individuals
will also be able to give or deny consent to a data collector to further process their data.
Collection of sensitive individual characteristics, such as those listed in Article 9 of the
Regulation (\emph{[...] racial or ethnic origin, political opinions, religious or
  philosophical beliefs, or trade union membership, and the processing of genetic data, biometric
  data for the purpose of uniquely identifying a natural person, data concerning health or data
  concerning a natural person's sex life or sexual orientation}) will be prohibited.
This should not be a problem for research, however, as Article 9 further states that such
processing of information is lawful if done \emph{ for archiving purposes in the public
  interest, scientific or historical research purposes or statistical purposes}.

Economic research is arguably in the public interest. The next subsections will present the data I
have used for this thesis in general terms. More details are then given in each chapter.

\subsection{The LIS data}

In the first chapter I estimate a hierarchical model using French and German micro-data.
Comparability issues may arise when using data, especially micro-data, from different
countries. To solve this problem, I use data from the LIS\footnote{In the past, LIS stood for
Luxembourg Income Study, which is not the case anymore.} Cross-National Data Center located in
Esch-Belval in Luxembourg. Access to the data is done remotely. Remote access is becoming more
widespread, and several models have emerged. In this section I will describe the model that the LIS
data center is using. In the next section I will describe the model of the CASD\footnote{CASD:
  \emph{Centre d'Accès Sécurisé aux Données}, Secure Data Access Center} which is located in
France. The CASD makes remote access to data from the INSEE and other institutes possible.
Any researcher can ask for access to the
data without needing to specify a project beforehand. There is no need to define a project
beforehand because researchers that get access to the data will never actually see it. Once the
researcher's demand for access is granted (which happens very fast), the researcher receives a
login as well as a password. With this, the researcher can login to LISSY; LISSY is a piece of
software written in JAVA through which it is possible to send computer code written in either R,
STATA or SAS. The code gets executed over at the LIS' servers, and the results are sent back to the
researcher, in almost real time. This is a lot faster than sending the code via email, which is
still a possibility. Compared to a model such as the one of the CASD, mere days go back from the
date access gets asked to when it is granted.
However, once the researcher can login through the CASD, working on the data is just like if it was
happening on a regular computer. The researcher can look at the data line by line, which cannot be
done with LISSY. For example, the following R command would tipycall print the first six lines of the
dataset:

\begin{verbatim}
head(lis_data)
\end{verbatim}

Calling this command in LISSY, however, produces nothing. The consequence of the researcher never looking at
the data makes it thus possible to avoid long and tedious procedures while protecting the privacy
of citizens. The way of accessing data is not the only difference though; the LIS offers access to
datasets to study the income of people in various countries, while the data accessible through the
CASD is only French data.\footnote{There is however the possibility to access German data from the
  IAB (Institute for Employment Research) at an access point installed in the CASD premises in
  Palaiseau. In Nüremberg, German researchers can access the French data available on the CASD
  platform from a secure access point installed in the premises of the IAB.}
The LIS team spends a lot of time and effort to make sure that the
datasets are comparable, as well as imputing missing values. LIS data is thus very well suited for
cross national comparisons, which is the justification of why I use it for the first chapter of
this thesis.

\subsection{The DADS-EDP data}

The DADS-EDP\footnote{DADS stands for \emph{Déclaration Annuelle des Données Sociales}, or Annual Declaration
  of Social data. EDP stands for \emph{Échantillon Démographique Permanent}, or Permanant
  Demographic sample.} data is data on French citizens and firms that is accessible through the CASD, a remote access
system that I have briefly discussed in the previous section. Access to this data requires more
patience than for the LIS, but this is explained by the fact that in the case of CASD data, the
researcher has access to it almost as if the data was stored locally. This is not the case however,
the data never exits the servers of the CASD, but working with data through the CASD is very
comfortable.  Researchers that wish to access the data need to submit a
proposal that has to be approved by a committee. Once the project is approved, the researchers need
to go to Paris to sign some more papers, and to have one of their fingerprint registered. The
fingerprint is needed to log-in to the system. The researchers also receive a crash course in
statistical disclosure control. Once this is done, researchers receive a special keyboard at their
institutes. Once the keyboard is plugged to the internet and to a computer screen, the researchers
can login to the system by using a keycard received at the training session and by presenting
their fingerprint to the fingerprint reader. Once this is done, the researcher is logged into a
Microsoft Windows virtual machine that contains the data the researcher asked access for as well as
standard statistical software, such as GNU R, SAS and STATA. Data for Chapter 2 and 3 were accessed
this way. This procedure might seem tedious, but is necessary to protect the privacy of
citizens; the LIS Data Center opted for an easier form of remote access.

\subsection{Version control to facilitate research}

Chapter \ref{chap5} is somewhat special as it does not deal with labour supply nor fertility nor
economics at all for that matter. It discusses a tool called Git that was of vital importance
to realise this thesis. This chapter is published as \citet{rodrigues2016}.

Git is a tool used for software carpentry or software development. In the last chapter
I discuss how research resembles Open Source software development and thus why using software
development tools is useful. Git allows to track changes of the source code of a piece of software,
or, in the case of a scientific article, changes made to the document. It is of course also
possible to track changes to the computer code that does the analysis. Git makes collaboration
extremely streamlined, and two of the papers that constitute this thesis have been co-authored with
Git. The source code to this thesis is also tracked using Git and the source code is available on
the following repository: \url{https://bitbucket.org/b-rodrigues/thesis2016/src/master/}.
The source code to the papers upon which Chapters 3 and 4 are based is also
available.\footnote{Chapter 3 paper: \url{https://bitbucket.org/b-rodrigues/diff-in-diff/src}, Chapter 4
  paper: \url{https://bitbucket.org/b-rodrigues/maternity_duration/src}. Note that there might be
slight differences in presentation between the these versions and the chapters of the thesis.}

\subsection{Questions that this thesis aims to answer}

This thesis studies labour supply and fertility decisions of women. Each chapter
deals with this topic, but from a different perspective. Chapter \ref{chap2} aims to see if there
are differences between French and German regions regarding total number of children women decide
to have. These two countries were chosen because they are
similar in many ways, but also differ greatly when in comes to women's participation to the labour
market and the fertility decisions. Chapter \ref{chap2} goes deeper in the analysis and considers the
regional level instead of the national level. Then, Chapter \ref{chap3} studies the impact of a
first, second and third birth on the labour supply and wages of both women and men. The question
here is to see to which extend these births impact negatively, or perhaps in the case of men even
positively, labour market outcomes. Chapter \ref{chap4} studies the duration of the maternity leave
and how mothers decide to come back to the labour market. How long are the breaks the mothers take
due to childbearing? And how do they come back? Do most women come back to full time work, or
rather part time work? Chapter \ref{chap3} and Chapter \ref{chap4} focus on France.

\bibliographystyle{plainnat}
\bibliography{bibliography}
