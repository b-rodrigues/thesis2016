% Chapter 1

\chapter{Version control systems to facilitate research collaboration in economics}
\label{chap5}
% Main chapter title

\lhead{Chapter 5. \emph{Version control systems to facilitate research collaboration in economics}}


\section{Introduction} \label{intro} Git is a version control system developed by Linus Torvalds
around 2005 to make developing the Linux kernel easier. The Linux kernel is without a doubt the
most successful piece of Free Software. The Free Software movement was officially started in 1983,
with the announcement of the GNU project by Richard
Stallman.\footnote{\url{https://www.gnu.org/gnu/initial-announcement.html}} Before the GNU project,
source code was usually distributed alongside compiled binaries, but when companies understood that
by not sharing the source code of their commercial software they could have a competitive advantage
over their competitors, this practice was stopped. In 1998, another movement, the Open Source
Initiative, was founded by Bruce Perens and Eric S. Raymond. The Open Source Initiative argues that
software for which the code is available will eventually perform better and be developed faster
thanks to the so-called Linus law, named after Linus Torvalds. If enough people can look at source
code and submit patches, eventually the software will be practically bugfree.

Interestingly, Free Software, or Open Source software development looks very much like scientific
development. Software developers usually start working because they want to \emph{scratch an
    itch}\footnote{This expression is used by Eric S. Raymond in his book, \emph{The Cathedral and
        the Bazaar} to explain the motivation software developers need to have to start working on
    a project.}, they check whether someone else worked on easing this itch, and if not, they
propose a solution of their own, or extend an existing solution that does not quite ease their
itch. It is very much the same for science; a researcher starts with a question, reads the related
literature on the matter, and if his curiosity is still not satisfied, he will work on extending
the current literature. This could not work without available literature for the researcher, or
available software and source code for the software developer. In recent years, the line between
the software developer and the researcher has been getting thinner; for an economist or
econometrician, a lot of time writing a paper is actually spent on writing code.  Cleaning data
sets, computing descriptive statistics, plotting data, running regression and then putting all
these results together and writing a paper using \LaTeX. The amount of code an economist or
econometrician writes for a paper today keeps getting bigger, more complicated and more
sophisticated. As \cite{koenker2009repecores} put it "[...] software development is no longer
something that should be left to specialized commercial developers, but instead should be an
integral part of the artisanal econometric research process." Authors also have been concerned with
reproducibility in economics for a long time now. \cite{dewald1986} tried to ask co-authors for
data so that they could replicate their findings. Most of the polled authors were either unable, or
unwilling, to share their data and source code. When authors did share their data and code, their
data was usually poorly documented (sometimes completely undocumented), and the source code was for
non free software that was not shareable (and the source code itself was often not very well
documented either). \citet{dewald1986} also find that errors in studies are the rule rather than the
exception. \citet{dewald1986} thus recommended to journals to urge authors to systematically share their
data and source code, and lament the lack of alternatives to non-free programs used at the time for
empirical research in economics. Almost thirty years later, much has changed, though. The
importance of sharing the source code to a paper has been recognized by some authors that host the
code to their papers on their own personal webpages. Even the Federal Reserve released the source
code to its internal DSGE model; critiques of the model and patches will surely follow in the next
few months, making the model more accurate and perform better. Journals also recommend, and
sometimes require, the authors to share their source code. \cite{stodden_journals} gives a detailed
overview of this growing trend among journals.

Tools have also evolved since \citet{dewald1986}. We now have powerful and credible free software
alternatives to non-free programs. Storing and sharing data is also much easier now than in 1986, a
time where floppy disks were ubiquitous but also not very reliable and the world wide web was not
invented yet.

 This paper will present Git, which solves most problems with collaborating with authors and with
 the sharing of source code to a paper. I will not argue why you should share your code; others
 such as \cite{topten} have done it before me, and there is not much I could add to their
 arguments.

\section{Scope of Git} Git is a distributed revision control system developed by Linus Torvalds in
2005 to make the collaboration of developers working on the Linux kernel easier. In 2012, the Linux
foundation released a document stating that since 2005 more than 7800 developers contributed to the
Linux kernel. Managing this amount of developers and patches seems impossible, and yet, the Linux
kernel has been worked on for more than 20 years now. This is made possible by revision control
systems such as Git.

For a lot of research teams, the current workflow is as follows: they set up a shared folder using
a file hosting service, and have to take turns to change the paper. Once one team member is done,
another collaborator can then change the paper and so on. If two or more people change the paper
simultaneously, there will be conflicts and either some of the changes will be lost, or there will
be as many versions of the paper as needed to save all the modifications. Someone then has to
manually merge the modifications back to a single file. There are a range of problems with this
workflow:

\begin{itemize}
\item Collaborators have to take turns to modify a file.
\item Some modifications can easily be overwritten and lost.
\item There is no simple way to revert some modifications.
\item After some time, it is quite hard to know who changed what.
\item There is no simple way to compare different versions of the same file.
\item If a team member has a new idea while writing the paper, he has to create a new folder
with the same files and modify this copy of the file.
\end{itemize}

Git (and other distributed revision control system tools) aims to solve these issues. With Git,
one's work never gets lost or overwritten. As long as a team member saved his file and
\emph{committed} his changes, it simply cannot be overwritten or erased. Team members can work
simultaneously on the same file: as long as the team members don't change the same lines, the
modifications will be merged without problems. Should the team members change the same lines, Git
helps to solve conflict between versions and more importantly, Git still keeps every change from
every team member. Reverting to an earlier version of the project is trivial as the whole history
of the project is saved automatically, with precise information on who changed what. Team members
can also easily check the differences between different versions of the project. Exploring new
ideas is also easily possible: instead of copying the project's folder structure and modify this
copy, a team member can create a new \emph{branch} and work on this branch without cluttering his
hard disk with copies of the project, and potentially lose track of his work. If the team members are
satisfied with the idea, the changes can then be merged back to the main branch, or they can simply
discard the experimental branch if the idea is not satisfactory.

The whole project can be hosted on different websites, for free. There are \url{www.github.com} and
\url{https://bitbucket.org/} for example. The main difference is that Bitbucket allows the creation
of private repositories, so the project's code is not accessible to people outside of the project.
Once the team is satisfied with the state of the project, or once they published their paper, they
can make the repository public (if they wish to share their code with the rest of the scientific
community). There are several reasons why it is better to host code on sites like Bitbucket and
Github instead of one of the team members' personal webpage:

\begin{itemize}
\item If the code is hosted on Bitbucket or Github, nobody on the team needs to have a personal
webpage, which is often difficult to do.
\item Bitbucket and Github virtually have no downtime.
\item Download links do not ever expire.
\item Collaboration is easier via Bitbucket or Github. People can comment on the project's code
and propose patches.
\item Discoverability of the code is increased. It is not always easy to find code on an author's page.
\item Finding future colleague to work on projects is easier. Websites such as Bitbucket or
Github are also very dynamic social networks.\footnote{Github hosts over one million code
repositories, and has 340.000 registered authors, \cite{dabbish2012social}.}
\end{itemize}

With code hosted on such websites, it is possible to find code and replicate a study in a matter of
minutes. Suggesting modifications or submitting patches is also very simple and done very fast. Git
is also useful when working on a paper alone. It allows the researcher to keep track of his changes
more easily and also revert to older versions.

\section{Basic usage of Git}

\begin{figure} \centering \includegraphics[scale=0.5]{Figures/chap5/Fig6.png} \caption{This figure shows the
        commits of a project on the website bitbucket.org where the project is hosted. Two authors
        are working on a paper, and it is possible to know exactly who did what and when.}
    \label{fig:1} \end{figure}

Installing Git is straightforward. On GNU+Linux operating systems, Git is often pre-installed, and
if not, can be easily installed via the system's package manager. To install Git on Windows, a
researcher can download the installer made by the msysGit
project\footnote{\url{http://msysgit.github.io}} and for OSX there is also a graphical
installer.\footnote{\url{http://sourceforge.net/projects/git-osx-installer/}}. Then the team
members can each create a personal account on Bitbucket or Github. By default Git is used with the
system's command line interface, but there are graphical interfaces for users that are not used to
working with commands. Showing how Git works would take too much space here; the interested reader
is invited to consult the online appendix for a tutorial on Git. In a few sentences however, the
idea is that the \emph{project leader} initializes the project using Git. This creates a folder in
which he puts the files needed for the paper, such as the data, computer code, etc. The next step
is to tell Git to \emph{track} these files (so Git knows the changes that were made to the files)
and then \emph{push} the project to the repository. The other team members can then \emph{pull} the
project and the changes that were, make their own modifications and push the changes back to the
server and so on. Figure \ref{fig:1} shows the commits log for a project on bitbucket.org. Using
the commit's hash (the number in the second column), it is possible to revert to the state the
project was at that point.


Table \ref{tab:1} summarizes the basic commands to use Git:

\begin{table}[ht!]
% table caption is above the table
\caption{Basic Git commands} \label{tab:1}       % Give a unique label
% For LaTeX tables use
\begin{tabular}{lll} \hline\noalign{\smallskip} Common commands & Common options &
    Usage\footnote{Mostly taken from Git's manual pages}\\

 \texttt{git init}  & None  & Initializes a repository on your local machine.\\
 \\
 \texttt{git log} & \texttt{-p -N} & This command shows commit\footnote{A commit contains the modifications a team
     member made to a file.} logs. \\
     && \texttt{-p} shows the differences \\
     && between commits and \texttt{-N} shows \\
     && the  last commits.\\ \\
  \texttt{git status}  & None & Shows the current
 status of your \\&& local working directory. Added files, \\&& untracked files, modified files,
 etc.\\ \\ \texttt{git add}  & \texttt{-n}  & This command adds the changes \\&&to the next commit.
 You can specify \\&& single files by giving their exact names, \\&& or you can add every change to
 the next \\&&commit with \texttt{git add .}. \\&&The \texttt{-n} option does \\&& not actually add
 the files. \\&& This option is also called \texttt{--dry-run}.\\ \\ \texttt{git commit}  &
 \texttt{-a -m} & This command commits the changes \\&& along with a log message describing \\&&
 the changes. The \texttt{-a} option automatically \\ && adds every modification to the commit \\&&
 (thus no need to run \texttt{git add .} before). \\&&The \texttt{-m} option allows the user \\&&
 to add a commit message.  \\ \\ \texttt{git push}  & \texttt{-n} & Pushes the commit to the remote
 \\&& repository. The \texttt{-n} option does \\&& not actually update \\&& the remote
 repository.\\ \noalign{\smallskip}\hline \end{tabular} \end{table}

\newpage

Researchers in other disciplines have already acknowledged the usefulness of version control
systems such as Git (\citealt{ram2013git} and \citealt{peng2011} for instance) and economists are
also starting to use Git and Github for their papers (\citealt{aruoba2014comparison}).
\footnote{You can access the authors' Github repository here:
    \url{https://github.com/jesusfv/Comparison-Programming-Languages-Economics}. All the source
    code discussed in the paper is available to download and numerous suggestions to make the code
    run faster have been made by other developers, that are thanked in the current version of the
    paper.} Git has a steep learning curve, and it may seem useless to learn to master it. But the
more complicated and the more researchers are invested in a project, the more Git streamlines the
workflow of the team. With just the few commands from Table \ref{tab:1} it is already possible to
start using Git efficiently. To learn more about Git, one can consult the online
documentation\footnote{\url{https://git-scm.com/documentation}} or read books about Git such as
\cite{loeliger2012version}.


\section{Conclusion} Version control systems such as Git are tools that make collaboration among
software developers very easy and fast. The current tools used by most research teams to
collaborate are not really suited because economists need to write always more complex source code
for their papers. Using file synchronization tools or e-mailing files around among co-authors can
get very messy and difficult to keep track of. A tool such as Git could very well be adopted by
economists (that practice applied econometrics or not) to help them collaborate with each other.
Mastering Git may be hard, because of its steep learning curve. But the benefits and time savings
in the long run more than make up for it. Hosting the code on a website such as Bitbucket also
makes sharing the source code to a paper very simple. Once the authors are ready (or required by a
journal) to share the code a paper, they can publish the repository, and anyone can download the
code on their machines, and propose patches to the authors if they find bugs. This paper only
presented Git, but other version control systems exist. Some examples are Mercurial and Apache
Subversion.


\newpage
\bibliographystyle{plainnat}
\bibliography{bibliography}
